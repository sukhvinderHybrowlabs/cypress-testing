/// <reference types = "cypress" />
describe('Hover Testing', () => {

    it.skip("Getting UI and testing Hover" , ()=>{

        cy.visit("https://rahulshettyacademy.com/AutomationPractice/");
        cy.wait(1500);

        cy.get(".mouse-hover-content").invoke("show");
        cy.contains("Top").click();
        cy.url().should("include" , "#top")


    })


    it.only("Getting UI and testing Hover by Using Plugin" , ()=>{

        cy.visit("https://option-a-website.netlify.app/");
        cy.wait(1500);
        cy.get('[href="service.html"]').realHover();
        cy.contains("Marketing").click()
    })

})

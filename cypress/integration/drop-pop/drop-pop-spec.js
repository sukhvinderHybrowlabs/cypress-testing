/// <reference types="Cypress" />


describe('Handling- Alert, Confirm and child tab ', function () {

    before(()=>{
        cy.visit("http://qaclickacademy.com/practice.php")
    })

    it('My FirstTest case', function () {

         //window:alert
         cy.on('window:alert', (str) => {
            //Mocha
            expect(str).to.equal('Hello , share this practice page and share your knowledge')
        })

        //window:confirm
        cy.on('window:confirm', (str) => {
            //Mocha
            expect(str).to.equal('Hello , Are you sure you want to confirm?')
        })

        //alert
        cy.get('#alertbtn').click()
        //confirm
        cy.get('[value="Confirm"]').click()

       

        cy.get('#opentab').invoke('removeAttr', 'target').click()

        cy.url().should('include', 'rahulshettyacademy')

        cy.go('back')



    })


})

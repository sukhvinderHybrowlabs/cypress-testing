/// <reference types="Cypress" />

describe('My Third Test Suite', function () {
    
    before(() => {
        // root-level hook
        // runs once before all tests 
        cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
      })

    it('My FirstTest case', function () {

        cy.get(".table-display td:nth-child(2)").each(function($el, index, $list){
            let text = $el.text();
            if(text === "WebServices / REST API Testing with SoapUI"){
                cy.get(".table-display td:nth-child(2)").eq(index).next().then((el)=>{
                    expect(el.text()).to.eq("35")
                })
            }
        })

    })


})


/// <reference types = "cypress" />
describe('Fixture Testing', () => {


    before(()=>{
        cy.fixture('example').as("data")
    })

    it("Filling data using fixture" , function(){
        cy.visit("https://rahulshettyacademy.com/angularpractice/")

        cy.get('input[name = "name"]:nth-child(2)').type(this.data.name)

    })

})